1. Create an activity folder and a solution.sql file inside of it. 
2.Given the following data, provide the answer to the following: 

A.) List the books Authored by Marjorie Green.

 Marjorie Green 

*au_id = 213-46-8915
*title id= BU1032, BU2075

	title id= BU1032
	Title= The Busy Executive's Database Guide
	type= business
	price=19.99
	pub_id=1389

	title id= BU2075
	Title= You Can Combat Computer Stress!
	type= business
	price=2.99
	pub_id=736

	 

B.) List the books Authored by Michael O'Leary.

 Michael O'Leary

*au_id = 267-41-2394 
*title id= {BU1111, TC7777}

	title id= BU1111
	title= Cooking with Computers}
	type= business
	price= 11.95
	pub_id = 1389

	title id= TC777
	type= null
	price= null
	pub_id = null

C.) Write the author/s of "The Busy Executives Database Guide".

 Marjorie Green 

D.) Identify the publisher of "But Is It User Friendly"

 Cheryl Carson

E.) List the books published by Agodata Infosystems.

 Agodata Infosystems

*pub_id = 1389

Agodata Infosystems = {The Busy Executive's Database Guide, Cooking with Computers, Straight Talk About Computers, But is it User Friendly?, Secrets of Silicon Valley, Net Etiquette}




3. Create SQL Syntax and Queries to create a database based on the ERD:  

CREATE DATABASE blog_db;

USE blog_db;

CREATE TABLE users(
id INT NOT NULL AUTO_INCREMENT,
email VARCHAR(100) NOT NULL,
password VARCHAR(300) NOT NULL,
datetime_created DATETIME NOT NULL,
PRIMARY KEY(id));


CREATE TABLE posts(
id INT NOT NULL AUTO_INCREMENT,
author_id INT NOT NULL,
title VARCHAR(500) NOT NULL,
content VARCHAR(5000),
datetime_posted DATETIME,
PRIMARY KEY(id),
CONSTRAINT author_id
FOREIGN KEY(author_id) REFERENCES users(id)
ON UPDATE CASCADE
ON DELETE RESTRICT);


CREATE TABLE post_comments(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
content VARCHAR(5000),
datetime_commented DATETIME,
PRIMARY KEY(id),
CONSTRAINT post_id
	FOREIGN KEY(post_id) REFERENCES posts(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
CONSTRAINT user_id
	FOREIGN KEY(user_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);


CREATE TABLE post_likes(
id INT NOT NULL AUTO_INCREMENT,
post_id INT NOT NULL,
user_id INT NOT NULL,
datetime_liked DATETIME,
PRIMARY KEY(id),
CONSTRAINT fk_post_id1
	FOREIGN KEY(post_id) REFERENCES posts(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT,
CONSTRAINT fk_user_id1
	FOREIGN KEY(user_id) REFERENCES users(id)
	ON UPDATE CASCADE
	ON DELETE RESTRICT
);



